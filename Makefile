include ${EPICS_ENV_PATH}/module.Makefile

AUTO_DEPENDENCIES = NO
USR_DEPENDENCIES += s7plc,1.2.0
USR_DEPENDENCIES += modbus,2.9.0-ESS1
MISCS = ${AUTOMISCS} $(addprefix misc/, creator)

USR_DEPENDENCIES += autosave
USR_DEPENDENCIES += synappsstd
MISCS += $(wildcard misc/*.req)
