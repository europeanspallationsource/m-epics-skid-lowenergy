epicsEnvSet("PREFIX",	"CWL-CWS01:Ctrl-IOC-001:")
require recsync,1.2.0
dbLoadRecords(reccaster.db, P=$(PREFIX))

# @field IPADDR
# @type STRING
# PLC IP address

# @field RECVTIMEOUT
# @type INTEGER
# PLC->EPICS receive timeout (ms), should be longer than frequency of PLC SND block trigger (REQ input)

# @field REQUIRE_cwl-cws01_ctrl-plc-001_VERSION
# @runtime YES
# @field SAVEFILE_DIR
# @type  STRING
# The directory where autosave should save files

# @field REQUIRE_cwl-cws01_ctrl-plc-001_PATH
# @runtime YES

# S7 port           : 2000
# Input block size  : 614 bytes
# Output block size : 0 bytes
# Endianness        : BigEndian
s7plcConfigure("cwl-cws01_ctrl-plc-001", $(IPADDR), 2000, 614, 0, 1, $(RECVTIMEOUT), 0)

# Modbus port       : 502
drvAsynIPPortConfigure("cwl-cws01_ctrl-plc-001", $(IPADDR):502, 0, 0, 1)

# Link type         : TCP/IP (0)
modbusInterposeConfig("cwl-cws01_ctrl-plc-001", 0, $(RECVTIMEOUT), 0)

# Slave address     : 0
# Function code     : 16 - Write Multiple Registers
# Addressing        : Absolute (-1)
# Data segment      : 2 words
drvModbusAsynConfigure("cwl-cws01_ctrl-plc-001write", "cwl-cws01_ctrl-plc-001", 0, 16, -1, 2, 0, 0, "S7-1500")

# Load plc interface database
dbLoadRecords("cwl-cws01_ctrl-plc-001.db", "PLCNAME=cwl-cws01_ctrl-plc-001, MODVERSION=0.2.1")

# Configure autosave
# Commented out autosave as it does not work in production environment
# Number of sequenced backup files to write
#save_restoreSet_NumSeqFiles(1)

# Specify directories in which to search for request files
#set_requestfile_path("$(REQUIRE_cwl-cws01_ctrl-plc-001_PATH)", "misc")

# Specify where the save files should be
#set_savefile_path("$(SAVEFILE_DIR)", "")

# Specify what save files should be restored
#set_pass0_restoreFile("cwl-cws01_ctrl-plc-001.sav")

# Create monitor set
#doAfterIocInit("create_monitor_set('cwl-cws01_ctrl-plc-001.req', 1, '')")
